import { React, useState } from "react";
import { LOCALES } from "../intl/Locales";
import { FormattedMessage } from "react-intl";
import { Data } from "../intl/Data";
import { IntlProvider } from "react-intl";
import HomeData from "./HomeData"

 const Home = () => {
  const lang = [
    { name: "English", code: LOCALES.ENGLISH },
    { name: "Italiana", code: LOCALES.ITALIAN },
    { name: "Russian", code: LOCALES.RUSSIAN },
    { name: "Spanish", code: LOCALES.SPANISH },
  ];

  const [Locale, setLocale] = useState(getInitialLocal());

  const handleChange = (e) => {
    setLocale(e.target.value);

    localStorage.setItem("locale", e.target.value);
  };

  function getInitialLocal() {
    const savedLocale = localStorage.getItem("locale");
    return savedLocale || LOCALES.ENGLISH;
  }

  return (
    <IntlProvider
      messages={Data[Locale]}
      locale={Locale}
      defaultLocale={LOCALES.ENGLISH}
    >
      <div>
        <div>
          <div></div>
          <div>
            <FormattedMessage id="lang" />{" "}
            <select onChange={handleChange} value={Locale}>
              {lang.map(({ name, code }) => (
                <option key={code} value={code}>
                  {name}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
      <HomeData/>
    </IntlProvider>
  );
};
export default Home
