import { FormattedMessage } from "react-intl";

const ContactData = () => {
  return (
    <div className="contact">
      <h1>
        <FormattedMessage id="contact" />
      </h1>
      <h4> Phone number : 
      <FormattedMessage id="phone"/>
      </h4>
      <h4> E-mail : 
      <FormattedMessage id="mail"/>
      </h4>
      
    </div>
  );
};

export default ContactData
