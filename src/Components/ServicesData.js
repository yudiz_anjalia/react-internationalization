import { FormattedMessage } from "react-intl";

const ServicesData = () => {
  return (
    <div className="services">
      <h1>
        <FormattedMessage id="info" />
      </h1>
      <h1>
        <FormattedMessage id="deep" />
      </h1>
      
    </div>
  );
};

export default ServicesData
