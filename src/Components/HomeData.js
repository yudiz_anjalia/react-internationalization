import { FormattedMessage } from "react-intl";

const HomeData = () => {
  return (
    <div className="home">
      <h1>
        <FormattedMessage id="title" />
      </h1>
    </div>
  );
};
export default HomeData