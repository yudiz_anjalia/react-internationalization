import { FormattedMessage } from "react-intl";

const AboutData = () => {
  return (
    <div className="about">
      <h2>
        <FormattedMessage id="currency" values={{ n: 100 }} />
      </h2>
      <h2>
        <FormattedMessage id="number" values={{ n: 500 }} />
      </h2>
      <h2>
        <FormattedMessage id="date" values={{ d: new Date() }} />
      </h2>
    </div>
  );
};

export default AboutData
