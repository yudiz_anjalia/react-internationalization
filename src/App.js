import './App.css';
import Contact from './Components/Contact';
import Home from './Components/Home';
import About from './Components/About';
import Services from './Components/Services';
import Navbar from './Navbar';

import {BrowserRouter as Router , Switch, Route} from 'react-router-dom';

function App() {
  return (
    <div className="App">
     
      
      
     <Router>
       <Navbar />
      <div>
       
      <Switch>
      
       
        <Route path = '/home' component = {Home} exact/>
        <Route path = '/contact' component = {Contact} exact />
        <Route path = '/about' component = {About} exact/> 
        <Route path = '/services' component = {Services} exact/>

      </Switch>
      </div>
      </Router>
      
    </div>
  );
}

export default App;